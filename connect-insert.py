# import the librairy pymongo to use python + MongoDB
import pymongo
# create client can connect to this host (127.0.0.1) and this port (27017)
client = pymongo.MongoClient("127.0.0.1", 27017)
# create database named (test-mongo-python)
database = client["test-mongo-python"]
# create collection named (les-employees)
collection = database["les-employees"]
# create a array of objects its jason format
employees = [{"Nom": "NAIMY", "PRENOM": "BOUCHAIB", "AGE": "20"},
{"Nom": "X", "PRENOM": "Y", "AGE": "22"},
{"Nom": "A", "PRENOM": "B", "AGE": "24"},
{"Nom": "C", "PRENOM": "D", "AGE": "26"}]
# save this objects in our collection in database
save = collection.insert_many(employees)
# its condition means that i want to find just the employee have in name (A)
condition1 = {"Nom": "A"}
# second condition find the document started by A
condition2 = {"Nom": {"$gt": "A"}}
# condition 3 find just the terme started by A
condition3 = {"Nom": {"$regex": "A"}}
# find this employee
show = collection.find_one(condition3)
show1 = collection.find().sort("Nom")
for x in show1 :
 print(x)
#print the result
#print(show)
# delete a object
# delete = {"Nom": "C", "PRENOM": "D", "AGE": "26"}
# deleted
# collection.delete_one(delete)
# delete all in the collection
# x = mycol.delete_many({})
# number of elements to deleted
# x.deleted_count

# drop the collection

# collection.drop()

# to_update = {"Nom": "C", "PRENOM": "D", "AGE": "26"}
# new_value = { "$set": { "Nom": "user" } }
# collection.update_one(to_update , new_value)
# for x in collection.find() :
# print(x)

# myquery = {"Nom": {"$regex": "^N"}}
# newvalues = {"$set": {"Nom": "user"}}

# x = collection.update_many(myquery, newvalues)
# print(x.modified_count, "documents updated.")

#myresult = collection.find().limit(5)